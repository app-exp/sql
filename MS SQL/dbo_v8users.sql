
--1
USE [DBName]
EXEC sp_rename 'v8users', 'v8users_old'
GO
UPDATE dbo.Params
SET FileName = 'users.usr_old'
WHERE FileName = 'users.usr'
GO

--2
--start configurator and open users' list (it must be empty)

--3
USE [DBName]
DROP TABLE v8users
GO
EXEC sp_rename 'v8users_old', 'v8users'
GO
UPDATE dbo.Params
SET FileName = 'users.usr'
WHERE FileName = 'users.usr_old'
GO
--users should appear

--4
--change the user's password